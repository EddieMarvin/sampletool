//
//  HelloTool.h
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/06/21.
//  Copyright © 2016年 Eddie LukeAtmey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainObjC : NSObject

+ (void)start;

@end

#import "MainObjC+TestCoreData.h"
