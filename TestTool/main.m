//
//  main.m
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/06/21.
//  Copyright © 2016年 Eddie LukeAtmey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainObjC.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");

        [MainObjC start];
    }
    return 0;
}
