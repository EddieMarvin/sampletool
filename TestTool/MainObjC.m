//
//  HelloTool.m
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/06/21.
//  Copyright © 2016年 Eddie LukeAtmey. All rights reserved.
//

#import "MainObjC.h"
#import "SomeClass.h"

@implementation MainObjC

+ (void)start
{
    NSString *testString;
    NSNumber *a = [NSNumber numberWithInteger:[testString integerValue]];

    NSLog(@"%@, %@", testString, a);
//    [self testCoreData];

//    [self testKVO];
//    [self simpleTest];
}

+ (void)testKVO
{
    NSMutableArray <SomeClass *> *arr = [[NSMutableArray alloc] initWithCapacity:10];
    for (int i = 0; i < 10; i++) {
        SomeClass *obj = [[SomeClass alloc] init];
        obj.stringProp = @(i).stringValue;
        [arr addObject:obj];
    }

    NSLog(@"- %@", [arr valueForKeyPath:@"stringProp"]);
    [arr setValuesForKeysWithDictionary:@{@"stringProp" : @"Same same!"}];

    NSLog(@"- %@", [arr valueForKeyPath:@"stringProp"]);
    [arr setValue:@"Another" forKeyPath:@"stringProp"];

    NSLog(@"- %@", [arr valueForKeyPath:@"stringProp"]);
}

- (void)testEncoding
{
    NSCharacterSet *URLCombinedCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:@" {}\"!#$&'()*+,/:;=?@[]%"] invertedSet];

    NSLog(@"%@", [@"returnUrl=ppxpos://handler?Type={Type}&InvoiceId={InvoiceId}&Tip={Tip}&Email={Email}&TxId={TxId}&Number={number} &accepted=cash,card,paypal&step=choosePayment&invoice={\"itemList\":{\"item\":[{\"name\":\"Coleslaw\",\"description\":\"My aunt`s coleslaw recipe\",\"quantity\":\"1\",\"unitPrice\":\"1.10\",\"taxRate\":\"5.0\",\"taxName\":\"NE Tax\"}]},\"paymentTerms\":\"DueOnReceipt\",\"currencyCode\":\"USD\",\"merchantEmail\":\"xxxxxxxxx@xxxxxxx.com\"}"
                  stringByAddingPercentEncodingWithAllowedCharacters:URLCombinedCharacterSet]);
}

- (void)testClass
{
    BOOL createClass = NO;
    Class class = createClass ? [SomeClass class] : [SomeChildClass class];

    SomeClass *hello = [[class alloc] init];

    [hello someOtherFunction];
}

- (void)testArray
{
    NSArray *testArray =  @[
                            @{
                                @"id": @1,
                                @"code": @"default",
                                @"name": @"English",
                                @"group_id": @1,
                                @"is_active": @YES,
                                @"pivot": @{
                                        @"user_id": @37,
                                        @"store_id": @1
                                        }
                                },
                            @{
                                @"id": @2,
                                @"code": @"french",
                                @"name": @"French",
                                @"group_id": @1,
                                @"is_active": @YES,
                                @"pivot": @{
                                        @"user_id": @37,
                                        @"store_id": @2
                                        }
                                },
                            @{
                                @"id": @3,
                                @"code": @"german",
                                @"name": @"German",
                                @"group_id": @1,
                                @"is_active": @YES,
                                @"pivot": @{
                                        @"user_id": @37,
                                        @"store_id": @3
                                        }
                                }
                            ];

    id wut = [testArray valueForKeyPath:@"id"];

    NSLog(@"%@ ", wut);
}

+ (void)simpleTest
{
// Write your simple test here.
}

@end
