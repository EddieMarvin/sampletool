//
//  CoreDataUtil.h
//  XPOS-iPad
//
//  Created by Ngoc Dang Thanh on 2015/04/24.
//  Copyright (c) 2015年 SmartOSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

/** CoreData Utility class */
@interface CoreDataUtil : NSObject

/**
 *  get the ManagedObjectContext
 *
 *  @return ManagedObjectContext. Auto init.
 */
+ (NSManagedObjectContext *)managedObjectContext;

/**
 *  Get a new ManagedObjectContext with the static context as parrent.
 *
 *  @return a new child ManagedObjectContext.
 */
+ (NSManagedObjectContext *)newManagedObjectContext;

/**
 *  Fetch all manageObject with its specified name.
 *
 *  @param  name            entity's name (E.g. Product, State...)
 *  @param  sortDescriptors SortDescriptor array.
 *  @return fetched objects in coredata.
 */
+ (NSArray *)fetchObjectsWithName:(NSString *)name sortDescriptors:(NSArray *)sortDescriptors;

/**
 *  Fetch all manageObject in the main context with its specified name and specified filter.
 *
 *  @param  name            entity's name (E.g. Product, State...)
 *  @param  predicate       filter the object
 *  @param  sortDescriptors SortDescriptor array.
 *  @return fetched objects in coredata.
 */
+ (NSArray *)fetchObjectsWithName:(NSString *)name filter:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors;

/**
 *  Fetch all manageObject with its specified name and specified filter.
 *
 *  @param  name            entity's name (E.g. Product, State...)
 *  @param  predicate       filter the object
 *  @param  sortDescriptors SortDescriptor array.
 *  @param  context the context to fetch data.
 *  @return fetched objects in coredata.
 */
+ (NSArray *)fetchObjectsWithName:(NSString *)name filter:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inContext:(NSManagedObjectContext *)context;

/**
 *  Count all manageObject with its specified name and specified filter.
 *
 *  @param  name            entity's name (E.g. Product, State...)
 *  @param  predicate       filter the object
 *  @return number of object satisfied the predicate.
 */
+ (NSInteger)countObjectsWithName:(NSString *)name filter:(NSPredicate *)predicate;

/**
 *  Create a FetchedResultsController.
 *
 *  @param  name                entity's name (E.g. Product, State...)
 *  @param  sortDescriptors     SortDescriptor array.
 *  @param  sectionNameKeyPath  section Key Path.
 *  @param  delegate            FetchedResultsController's Delegate.
 *  @return the created FetchedResultsController.
 */
+ (NSFetchedResultsController *)performFetchResultsWithName:(NSString *)name sortDescriptors:(NSArray *)sortDescriptors sectionNameKeyPath:(NSString *)sectionNameKeyPath delegate:(id <NSFetchedResultsControllerDelegate>)delegate;

/**
 *  Get FetchRequest.
 *
 *  @param  name            entity's name (E.g. Product, State...)
 *  @param  sortDescriptors SortDescriptor array.
 *  @return fetch request
 */
+ (NSFetchRequest *)fetchRequestWithName:(NSString *)name sortDescriptors:(NSArray *)sortDescriptors;

/**
 *  Insert new object into coredata.
 *
 *  @param  name    entity's name.
 *  @return inserted object。
 */
+ (id)insertNewObjectForName:(NSString *)name;

/**
 *  Insert new object into coredata with the specified managed object context.
 *
 *  @param  name   entity's name.
 *  @param context the specified managed object context.
 *  @return inserted object。
 */
+ (id)insertNewObjectForName:(NSString *)name inContext:(NSManagedObjectContext *)context;

/** Insert dummy object, which won't affect the database. */
+ (id)insertDummyObjectForName:(NSString *)name;

/**
 *  Delete object from coredata.
 *
 *  @param  object  object to be delete.
 */
+ (void)deleteObject:(NSManagedObject *)object;

/**
 *  Delete multiple objects from coredata.
 *
 *  @param  objects the list of object to be delete.
 */
+ (void)deleteObjects:(NSArray *)objects;

/** Hard reset coredata. */
+ (void)resetCoreData;

/**
 *  Save change in the main context.
 *
 *  @returns YES if saved. NO if error occurs.
 */
+ (void)save;

/**
 *  Save change in the specified context.
 *
 *  @returns YES if saved. NO if error occurs.
 */
+ (void)saveContext:(NSManagedObjectContext *)context;

@end

@interface NSManagedObject (DummyObject)

/** Create a dummy object (which won't affect the database because context = nil) with properties from a base managedObject. */
- (instancetype)dummyObject;

@end
