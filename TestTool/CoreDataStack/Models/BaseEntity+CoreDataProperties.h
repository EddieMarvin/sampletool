//
//  BaseEntity+CoreDataProperties.h
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "BaseEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BaseEntity (CoreDataProperties)

+ (NSFetchRequest<BaseEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name1;
@property (nullable, nonatomic, copy) NSString *name2;

@end

NS_ASSUME_NONNULL_END
