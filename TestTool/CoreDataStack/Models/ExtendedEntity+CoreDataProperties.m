//
//  ExtendedEntity+CoreDataProperties.m
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "ExtendedEntity+CoreDataProperties.h"

@implementation ExtendedEntity (CoreDataProperties)

+ (NSFetchRequest<ExtendedEntity *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ExtendedEntity"];
}

@dynamic extendedName;

@end
