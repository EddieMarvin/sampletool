//
//  BaseEntity+CoreDataProperties.m
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "BaseEntity+CoreDataProperties.h"

@implementation BaseEntity (CoreDataProperties)

+ (NSFetchRequest<BaseEntity *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BaseEntity"];
}

@dynamic name1;
@dynamic name2;

@end
