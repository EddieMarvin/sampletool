//
//  ExtendedEntity+CoreDataClass.h
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import "BaseEntity+CoreDataClass.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExtendedEntity : BaseEntity

@end

NS_ASSUME_NONNULL_END

#import "ExtendedEntity+CoreDataProperties.h"
