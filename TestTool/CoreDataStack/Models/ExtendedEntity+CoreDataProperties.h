//
//  ExtendedEntity+CoreDataProperties.h
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import "ExtendedEntity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ExtendedEntity (CoreDataProperties)

+ (NSFetchRequest<ExtendedEntity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *extendedName;

@end

NS_ASSUME_NONNULL_END
