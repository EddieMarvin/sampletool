//
//  BaseEntity+CoreDataClass.h
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseEntity : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BaseEntity+CoreDataProperties.h"
