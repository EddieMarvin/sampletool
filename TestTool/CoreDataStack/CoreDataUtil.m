//
//  CoreDataUtil.m
//  XPOS-iPad
//
//  Created by Ngoc Dang Thanh on 2015/04/24.
//  Copyright (c) 2015年 SmartOSC. All rights reserved.
//

#import "CoreDataUtil.h"

@implementation CoreDataUtil

#pragma mark - getting config

///** ManagedObjectContext。 */
static NSManagedObjectContext *_managedObjectContext;
static NSManagedObjectContext *_masterManagedObjectContext;
static NSPersistentStoreCoordinator *_persistentStoreCoordinator;

NSString * const MODEL_NAME = @"Model";

+ (NSManagedObjectContext *)masterManagedObjectContext
{
    if (_masterManagedObjectContext) {
        return _masterManagedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self createPersistentStoreCoordinator];

    if (coordinator != nil) {
        _masterManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        _masterManagedObjectContext.retainsRegisteredObjects = YES;
        [_masterManagedObjectContext performBlockAndWait:^{
            [_masterManagedObjectContext setPersistentStoreCoordinator:coordinator];
        }];

    }
    return _masterManagedObjectContext;
}

+ (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext) {
        return _managedObjectContext;
    }

    NSManagedObjectContext *masterContext = [self masterManagedObjectContext];

    if (masterContext) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        _masterManagedObjectContext.retainsRegisteredObjects = YES;

        [masterContext performBlockAndWait:^{
            [masterContext setMergePolicy:NSOverwriteMergePolicy];
        }];

        [_managedObjectContext performBlockAndWait:^{
            [_managedObjectContext setParentContext:masterContext];
        }];
    }

    return _managedObjectContext;
}

+ (NSManagedObjectContext *)newManagedObjectContext
{
    NSManagedObjectContext *newContext = nil;
    NSManagedObjectContext *parentContext = [self managedObjectContext];

    if (parentContext) {
        newContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [parentContext performBlockAndWait:^{
            [parentContext setMergePolicy:NSOverwriteMergePolicy];
        }];
        [newContext performBlockAndWait:^{
            [newContext setParentContext:parentContext];
        }];
    }

    return newContext;
}

/**
 *  Create a PersistentStoreCoordinator.
 *
 *  @return the created PersistentStoreCoordinator.
 */
+ (NSPersistentStoreCoordinator *)createPersistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    NSURL *storeDATAURL = [self storeURL];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];

    NSError *error = nil;

    // Check if need migration
    if (![[self managedObjectModel] isConfiguration:nil compatibleWithStoreMetadata:[NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType URL:storeDATAURL error:&error]] ) {

#warning Show notify to user migration is taking place.

    }
    else {
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeDATAURL options:nil error:&error];
        return _persistentStoreCoordinator;
    }

    // execute migration on background queue.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void){

        dispatch_async(dispatch_get_main_queue(), ^{
            [_persistentStoreCoordinator performBlockAndWait:^{
                NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption : @YES,
                                          NSInferMappingModelAutomaticallyOption : @YES};
                NSError *error = nil;

                if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeDATAURL options:options error:&error]) {
                    NSLog(@"Migrate error: %@", error);

                    // Hard reset coreData.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self resetCoreData];
#warning Show message
                    });
                }
                else {
                }
            }]; 
        });
    });
    
    return _persistentStoreCoordinator;
}

/**
 * Get Coredata storeURL.
 *
 *  @return coredata storeURL.
 */
+ (NSURL *)storeURL
{
    return [[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", MODEL_NAME]];
}

/**
 *  Create a ManagedObjectModel
 *
 *  @return the created ManagedObjectModel.
 */
+ (NSManagedObjectModel *)managedObjectModel
{
    return [[NSManagedObjectModel alloc] initWithContentsOfURL:[[NSBundle mainBundle] URLForResource:MODEL_NAME withExtension:@"momd" ]];
}

#pragma mark - fetching methods
+ (NSArray *)fetchObjectsWithName:(NSString *)name sortDescriptors:(NSArray *)sortDescriptors
{
    NSError *error;

    NSArray *objects = [[self managedObjectContext] executeFetchRequest:[self fetchRequestWithName:name sortDescriptors:sortDescriptors] error:&error];
    if (!objects) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return objects;
}

+ (NSArray *)fetchObjectsWithName:(NSString *)name filter:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors
{
    return [self fetchObjectsWithName:name filter:predicate sortDescriptors:sortDescriptors inContext:_managedObjectContext];
}

+ (NSArray *)fetchObjectsWithName:(NSString *)name filter:(NSPredicate *)predicate sortDescriptors:(NSArray *)sortDescriptors inContext:(NSManagedObjectContext *)context
{
    NSError *error;

    NSFetchRequest *request = [self fetchRequestWithName:name sortDescriptors:sortDescriptors];
    request.predicate = predicate;

    NSArray *objects = [context executeFetchRequest:request error:&error];

    if (!objects) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return objects;
}

+ (NSInteger)countObjectsWithName:(NSString *)name filter:(NSPredicate *)predicate
{
    NSError *error;

    NSFetchRequest *request = [self fetchRequestWithName:name sortDescriptors:nil];
    request.predicate = predicate;
    request.resultType = NSCountResultType;
    request.includesSubentities = NO;
    request.includesPropertyValues = NO;

    NSInteger result = [[self managedObjectContext] countForFetchRequest:request error:&error];

    if (error) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        return 0;
    }

    return result == NSNotFound ? 0 : result;
}

+ (NSFetchedResultsController *)performFetchResultsWithName:(NSString *)name sortDescriptors:(NSArray *)sortDescriptors sectionNameKeyPath:(NSString *)sectionNameKeyPath delegate:(id <NSFetchedResultsControllerDelegate>)delegate
{
    NSError *error = nil;

    NSFetchedResultsController *fetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:[self fetchRequestWithName:name sortDescriptors:sortDescriptors] managedObjectContext:[self managedObjectContext] sectionNameKeyPath:sectionNameKeyPath cacheName:@"Master"];

    [fetchedResultsController setDelegate:delegate];
    if (![fetchedResultsController performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return fetchedResultsController;
}

+ (NSFetchRequest *)fetchRequestWithName:(NSString *)name sortDescriptors:(NSArray *)sortDescriptors
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:name];

    if (sortDescriptors)
        [fetchRequest setSortDescriptors:sortDescriptors];

    return fetchRequest;
}

+ (id)insertNewObjectForName:(NSString *)name
{
    return [self insertNewObjectForName:name inContext:[self managedObjectContext]];
}

+ (id)insertNewObjectForName:(NSString *)name inContext:(NSManagedObjectContext *)context
{
    if (!context)
        context = [self managedObjectContext];

    return [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:context];
}

+ (id)insertDummyObjectForName:(NSString *)name
{
    NSEntityDescription *entity = [NSEntityDescription entityForName:name inManagedObjectContext:[self managedObjectContext]];
    return [[NSClassFromString(name) alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
}

+ (void)deleteObject:(NSManagedObject *)object
{
    [[self managedObjectContext] deleteObject:object];
}

+ (void)deleteObjects:(NSArray *)objects
{
    for (NSManagedObject *object in objects) {
        [self deleteObject:object];
    }
}

+ (void)save
{
    @try {
        [self saveContext:[self managedObjectContext]];
    }
    @catch (NSException *exception) {
        NSLog(@"save manageObjectContext exeption:%@", [exception description]);
    }
}

+ (void)saveContext:(NSManagedObjectContext *)context
{
    [context performBlockAndWait:^{
        if (context.hasChanges && context.persistentStoreCoordinator.persistentStores.count) {
            NSError *error = nil;
            if ([context save:&error]) {
                // Recursive save parent context.
                if (context.parentContext) [self saveContext:context.parentContext];
            }
            else {
                // do some real error handling
                NSLog(@"Could not save master context due to %@", error);
            }
        }
    }];
}

+ (void)resetCoreData
{
    NSURL *storeURL = [self storeURL];
    [_managedObjectContext performBlockAndWait:^{
        [_managedObjectContext reset];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;

        for (NSPersistentStore *store in [_persistentStoreCoordinator persistentStores]) {
            [_persistentStoreCoordinator removePersistentStore:store error:&error];
            [fileManager removeItemAtURL:store.URL error:NULL];
        }

        [fileManager removeItemAtURL:storeURL error:NULL];

        _managedObjectContext = nil;
        _masterManagedObjectContext = nil;
        _persistentStoreCoordinator = nil;

        [self managedObjectContext];
    }];
}

@end

@implementation NSManagedObject (DummyObject)

- (instancetype)dummyObject
{
    if (!self.managedObjectContext) return self;

    NSManagedObject *dummyObject = [CoreDataUtil insertDummyObjectForName:self.entity.name];
    [dummyObject setValuesForKeysWithDictionary:[self dictionaryWithValuesForKeys:self.entity.attributesByName.allKeys]];

    return dummyObject;
}

@end
