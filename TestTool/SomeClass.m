//
//  SomeClass.m
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/07/01.
//  Copyright © 2016年 Eddie LukeAtmey. All rights reserved.
//

#import "SomeClass.h"

@implementation SomeClass

- (void)someOtherFunction
{
    [self someFunction];
}

- (void)someFunction
{
    NSLog(@"SomeClass someFunction");
}

@end

@implementation SomeChildClass

- (void)someFunction
{
    NSLog(@"SomeChildClass someFunction");
}

@end
