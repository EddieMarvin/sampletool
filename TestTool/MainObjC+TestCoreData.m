//
//  MainObjC+TestCoreData.m
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//

#import "MainObjC+TestCoreData.h"
#import "CoreDataUtil.h"

#import "BaseEntity+CoreDataClass.h"
#import "ExtendedEntity+CoreDataClass.h"

@implementation MainObjC (TestCoreData)

+ (void)testCoreData
{
    [self setup];

    NSArray <BaseEntity *> *arr1 = [CoreDataUtil fetchObjectsWithName:NSStringFromClass([BaseEntity class]) sortDescriptors:nil];
    NSArray <ExtendedEntity *> *arr2 = [CoreDataUtil fetchObjectsWithName:NSStringFromClass([ExtendedEntity class]) sortDescriptors:nil];

    NSAssert([arr1 containsObject:arr2.firstObject], @"");

    for (BaseEntity *entity in arr1) {
        if (entity.class == [ExtendedEntity class]) {
            ExtendedEntity *extEntity = (ExtendedEntity *)entity;
            NSLog(@"%@", extEntity.extendedName);
        }
    }
}

+ (void)setup
{
    // Setup stack
    [CoreDataUtil managedObjectContext];

    BaseEntity *entity = [CoreDataUtil insertNewObjectForName:NSStringFromClass([BaseEntity class])];
    entity.name1 = @"name1.1";
    entity.name2 = @"name1.2";

    entity = [CoreDataUtil insertNewObjectForName:NSStringFromClass([BaseEntity class])];
    entity.name1 = @"name2.1";
    entity.name2 = @"name2.2";

    entity = [CoreDataUtil insertNewObjectForName:NSStringFromClass([BaseEntity class])];
    entity.name1 = @"name3.1";
    entity.name2 = @"name3.2";

    ExtendedEntity *extEntity = [CoreDataUtil insertNewObjectForName:NSStringFromClass([ExtendedEntity class])];
    extEntity.name1 = @"name4.1";
    extEntity.name2 = @"name4.2";
    extEntity.extendedName = @"ext4";

    [CoreDataUtil save];
}

@end
