//
//  MainObjC+TestCoreData.h
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/11/07.
//  Copyright © 2016 Eddie LukeAtmey. All rights reserved.
//

#import "MainObjC.h"

@interface MainObjC (TestCoreData)

+ (void)testCoreData;

@end
