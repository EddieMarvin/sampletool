//
//  SomeClass.h
//  TestTool
//
//  Created by Eddie LukeAtmey on 2016/07/01.
//  Copyright © 2016年 Eddie LukeAtmey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SomeClass : NSObject

- (void)someOtherFunction;
- (void)someFunction;

@property (strong, nonatomic) NSString *stringProp;

@end

@interface SomeChildClass : SomeClass

@end

