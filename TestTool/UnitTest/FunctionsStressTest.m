//
//  FunctionsStressTest.m
//  TestTool
//
//  Created by Eddie LukeAtmey on 2017/01/06.
//  Copyright © 2017 Eddie LukeAtmey. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface FunctionsStressTest : XCTestCase

@property (strong, nonatomic) NSSet <NSString *> *testSample;

@end

NSString * const FIND_OBJECT = @"13123";
NSUInteger const MAX_VALUE = 0xFFFFFFF;

@implementation FunctionsStressTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        NSMutableSet *mset = [[NSMutableSet alloc] initWithCapacity:MAX_VALUE];

        for (NSUInteger i = 0; i < MAX_VALUE; i++) {
            [mset addObject:@(i).stringValue];
        }

        self.testSample = mset;
    });
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
//}

#pragma mark - NSSet
- (void)testPerformanceForIf {

    [self measureBlock:^{
        for (NSString *s in self.testSample) {
            if ([s isEqualToString:FIND_OBJECT]) NSLog(@"%@", s); return;
        }
    }];
}

- (void)testPerformancePredicateSet {

    [self measureBlock:^{
        NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF == %@", FIND_OBJECT];
        NSString *obj = [self.testSample filteredSetUsingPredicate:pre].allObjects.firstObject;
        NSLog(@"%@", obj);
    }];
}

- (void)testPerformancePassingTestSet {

    [self measureBlock:^{

        NSString *s = [self.testSample objectsPassingTest:^BOOL(NSString * _Nonnull obj, BOOL * _Nonnull stop) {
            if ([obj isEqualToString:FIND_OBJECT]) {
                *stop = YES;
                return YES;
            }
            return NO;

        }].allObjects.firstObject;

        NSLog(@"%@", s);
    }];

}

#pragma mark - NSArray

- (void)testPerformancePredicateArray {
    __block NSArray *convertedObj = self.testSample.allObjects;

    [self measureBlock:^{
        NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF == %@", FIND_OBJECT];
        NSString *obj = [convertedObj filteredArrayUsingPredicate:pre].firstObject;
        NSLog(@"%@", obj);
    }];
}

- (void)testPerformancePassingTestArray {
    __block NSArray *convertedObj = self.testSample.allObjects;

    [self measureBlock:^{

        NSUInteger index = [convertedObj indexOfObjectPassingTest:^BOOL(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return [obj isEqualToString:FIND_OBJECT];
        }];

        if (index != NSNotFound) {
            NSString *obj = [convertedObj objectAtIndex:index];
            NSLog(@"%@", obj);
        }
    }];

}

@end
